﻿namespace Economica
{
    partial class Formulario
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formulario));
            this.BarraSuperior = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.PictureBox();
            this.txtcapitalFinal = new System.Windows.Forms.TextBox();
            this.txtIntereses = new System.Windows.Forms.TextBox();
            this.txtTasaPorcentual = new System.Windows.Forms.TextBox();
            this.chkIntereses = new System.Windows.Forms.CheckBox();
            this.chkTasa_Porcentual = new System.Windows.Forms.CheckBox();
            this.chkCapital_Final = new System.Windows.Forms.CheckBox();
            this.txtCapitalInicial = new System.Windows.Forms.TextBox();
            this.chkCapita_Inicial = new System.Windows.Forms.CheckBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.btncalcular = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtPeriodo = new System.Windows.Forms.TextBox();
            this.cmbPeriodo = new System.Windows.Forms.ComboBox();
            this.BarraSuperior.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).BeginInit();
            this.SuspendLayout();
            // 
            // BarraSuperior
            // 
            this.BarraSuperior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.BarraSuperior.Controls.Add(this.btnExit);
            this.BarraSuperior.Dock = System.Windows.Forms.DockStyle.Top;
            this.BarraSuperior.Location = new System.Drawing.Point(0, 0);
            this.BarraSuperior.Name = "BarraSuperior";
            this.BarraSuperior.Size = new System.Drawing.Size(320, 35);
            this.BarraSuperior.TabIndex = 0;
            this.BarraSuperior.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BarraSuperior_MouseDown_1);
            // 
            // btnExit
            // 
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.Location = new System.Drawing.Point(289, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(27, 27);
            this.btnExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExit.TabIndex = 0;
            this.btnExit.TabStop = false;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // txtcapitalFinal
            // 
            this.txtcapitalFinal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtcapitalFinal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(219)))), ((int)(((byte)(221)))));
            this.txtcapitalFinal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtcapitalFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcapitalFinal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.txtcapitalFinal.Location = new System.Drawing.Point(201, 125);
            this.txtcapitalFinal.Name = "txtcapitalFinal";
            this.txtcapitalFinal.Size = new System.Drawing.Size(98, 19);
            this.txtcapitalFinal.TabIndex = 33;
            this.txtcapitalFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcapitalFinal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtcapitalFinal_KeyPress);
            // 
            // txtIntereses
            // 
            this.txtIntereses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIntereses.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(219)))), ((int)(((byte)(221)))));
            this.txtIntereses.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtIntereses.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIntereses.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.txtIntereses.Location = new System.Drawing.Point(199, 219);
            this.txtIntereses.Name = "txtIntereses";
            this.txtIntereses.Size = new System.Drawing.Size(100, 19);
            this.txtIntereses.TabIndex = 32;
            this.txtIntereses.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtIntereses.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtIntereses_KeyPress);
            // 
            // txtTasaPorcentual
            // 
            this.txtTasaPorcentual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTasaPorcentual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(219)))), ((int)(((byte)(221)))));
            this.txtTasaPorcentual.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTasaPorcentual.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTasaPorcentual.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.txtTasaPorcentual.Location = new System.Drawing.Point(199, 168);
            this.txtTasaPorcentual.Name = "txtTasaPorcentual";
            this.txtTasaPorcentual.Size = new System.Drawing.Size(100, 19);
            this.txtTasaPorcentual.TabIndex = 31;
            this.txtTasaPorcentual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTasaPorcentual.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtTasaPorcentual_KeyPress);
            // 
            // chkIntereses
            // 
            this.chkIntereses.AutoSize = true;
            this.chkIntereses.BackColor = System.Drawing.Color.Transparent;
            this.chkIntereses.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.chkIntereses.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkIntereses.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.chkIntereses.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIntereses.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.chkIntereses.Location = new System.Drawing.Point(21, 222);
            this.chkIntereses.Name = "chkIntereses";
            this.chkIntereses.Size = new System.Drawing.Size(105, 24);
            this.chkIntereses.TabIndex = 30;
            this.chkIntereses.Text = "INTERESES";
            this.chkIntereses.UseVisualStyleBackColor = false;
            this.chkIntereses.CheckedChanged += new System.EventHandler(this.ChkIntereses_CheckedChanged);
            // 
            // chkTasa_Porcentual
            // 
            this.chkTasa_Porcentual.AutoSize = true;
            this.chkTasa_Porcentual.BackColor = System.Drawing.Color.Transparent;
            this.chkTasa_Porcentual.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.chkTasa_Porcentual.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkTasa_Porcentual.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.chkTasa_Porcentual.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTasa_Porcentual.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.chkTasa_Porcentual.Location = new System.Drawing.Point(21, 172);
            this.chkTasa_Porcentual.Name = "chkTasa_Porcentual";
            this.chkTasa_Porcentual.Size = new System.Drawing.Size(85, 24);
            this.chkTasa_Porcentual.TabIndex = 29;
            this.chkTasa_Porcentual.Text = "TASA %";
            this.chkTasa_Porcentual.UseVisualStyleBackColor = false;
            this.chkTasa_Porcentual.CheckedChanged += new System.EventHandler(this.ChkTasa_Porcentual_CheckedChanged);
            // 
            // chkCapital_Final
            // 
            this.chkCapital_Final.AutoSize = true;
            this.chkCapital_Final.BackColor = System.Drawing.Color.Transparent;
            this.chkCapital_Final.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.chkCapital_Final.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkCapital_Final.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.chkCapital_Final.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCapital_Final.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.chkCapital_Final.Location = new System.Drawing.Point(21, 126);
            this.chkCapital_Final.Name = "chkCapital_Final";
            this.chkCapital_Final.Size = new System.Drawing.Size(144, 24);
            this.chkCapital_Final.TabIndex = 28;
            this.chkCapital_Final.Text = "VALOR FUTURO";
            this.chkCapital_Final.UseVisualStyleBackColor = false;
            this.chkCapital_Final.CheckedChanged += new System.EventHandler(this.ChkCapital_Final_CheckedChanged);
            // 
            // txtCapitalInicial
            // 
            this.txtCapitalInicial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCapitalInicial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(219)))), ((int)(((byte)(221)))));
            this.txtCapitalInicial.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCapitalInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCapitalInicial.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.txtCapitalInicial.Location = new System.Drawing.Point(199, 79);
            this.txtCapitalInicial.Name = "txtCapitalInicial";
            this.txtCapitalInicial.Size = new System.Drawing.Size(100, 19);
            this.txtCapitalInicial.TabIndex = 27;
            this.txtCapitalInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCapitalInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtCapitalInicial_KeyPress_1);
            // 
            // chkCapita_Inicial
            // 
            this.chkCapita_Inicial.AutoSize = true;
            this.chkCapita_Inicial.BackColor = System.Drawing.Color.Transparent;
            this.chkCapita_Inicial.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.chkCapita_Inicial.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkCapita_Inicial.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.chkCapita_Inicial.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCapita_Inicial.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.chkCapita_Inicial.Location = new System.Drawing.Point(21, 82);
            this.chkCapita_Inicial.Name = "chkCapita_Inicial";
            this.chkCapita_Inicial.Size = new System.Drawing.Size(156, 24);
            this.chkCapita_Inicial.TabIndex = 26;
            this.chkCapita_Inicial.Text = "VALOR PRESENTE";
            this.chkCapita_Inicial.UseVisualStyleBackColor = false;
            this.chkCapita_Inicial.CheckedChanged += new System.EventHandler(this.ChkCapita_Inicial_CheckedChanged);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape5,
            this.lineShape4,
            this.lineShape3,
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(320, 375);
            this.shapeContainer1.TabIndex = 34;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape5
            // 
            this.lineShape5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.lineShape5.Name = "lineShape5";
            this.lineShape5.X1 = 196;
            this.lineShape5.X2 = 241;
            this.lineShape5.Y1 = 280;
            this.lineShape5.Y2 = 280;
            // 
            // lineShape4
            // 
            this.lineShape4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 196;
            this.lineShape4.X2 = 299;
            this.lineShape4.Y1 = 240;
            this.lineShape4.Y2 = 240;
            // 
            // lineShape3
            // 
            this.lineShape3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 198;
            this.lineShape3.X2 = 300;
            this.lineShape3.Y1 = 187;
            this.lineShape3.Y2 = 187;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 195;
            this.lineShape2.X2 = 300;
            this.lineShape2.Y1 = 144;
            this.lineShape2.Y2 = 144;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 197;
            this.lineShape1.X2 = 300;
            this.lineShape1.Y1 = 100;
            this.lineShape1.Y2 = 100;
            // 
            // btncalcular
            // 
            this.btncalcular.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.btncalcular.FlatAppearance.BorderSize = 0;
            this.btncalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncalcular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncalcular.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(219)))), ((int)(((byte)(221)))));
            this.btncalcular.Location = new System.Drawing.Point(196, 324);
            this.btncalcular.Name = "btncalcular";
            this.btncalcular.Size = new System.Drawing.Size(75, 23);
            this.btncalcular.TabIndex = 35;
            this.btncalcular.Text = "Calcular";
            this.btncalcular.UseVisualStyleBackColor = false;
            this.btncalcular.Click += new System.EventHandler(this.BtnCalcular_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.checkBox1.Location = new System.Drawing.Point(21, 263);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(96, 22);
            this.checkBox1.TabIndex = 36;
            this.checkBox1.Text = "PERIODO";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // txtPeriodo
            // 
            this.txtPeriodo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPeriodo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(219)))), ((int)(((byte)(221)))));
            this.txtPeriodo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPeriodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPeriodo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.txtPeriodo.Location = new System.Drawing.Point(191, 260);
            this.txtPeriodo.Name = "txtPeriodo";
            this.txtPeriodo.Size = new System.Drawing.Size(42, 19);
            this.txtPeriodo.TabIndex = 37;
            this.txtPeriodo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cmbPeriodo
            // 
            this.cmbPeriodo.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.cmbPeriodo.AllowDrop = true;
            this.cmbPeriodo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(219)))), ((int)(((byte)(221)))));
            this.cmbPeriodo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbPeriodo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPeriodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPeriodo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.cmbPeriodo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cmbPeriodo.Items.AddRange(new object[] {
            "Dias. ",
            "Meses.",
            "Años."});
            this.cmbPeriodo.Location = new System.Drawing.Point(245, 257);
            this.cmbPeriodo.Name = "cmbPeriodo";
            this.cmbPeriodo.Size = new System.Drawing.Size(66, 24);
            this.cmbPeriodo.TabIndex = 38;
            this.cmbPeriodo.SelectedIndexChanged += new System.EventHandler(this.CmbPeriodo_SelectedIndexChanged);
            // 
            // Formulario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(219)))), ((int)(((byte)(221)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(320, 375);
            this.Controls.Add(this.cmbPeriodo);
            this.Controls.Add(this.txtPeriodo);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.btncalcular);
            this.Controls.Add(this.txtcapitalFinal);
            this.Controls.Add(this.txtIntereses);
            this.Controls.Add(this.txtTasaPorcentual);
            this.Controls.Add(this.chkIntereses);
            this.Controls.Add(this.chkTasa_Porcentual);
            this.Controls.Add(this.chkCapital_Final);
            this.Controls.Add(this.txtCapitalInicial);
            this.Controls.Add(this.chkCapita_Inicial);
            this.Controls.Add(this.BarraSuperior);
            this.Controls.Add(this.shapeContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Formulario";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.BarraSuperior.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnExit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel BarraSuperior;
        private System.Windows.Forms.PictureBox btnExit;
        private System.Windows.Forms.TextBox txtcapitalFinal;
        private System.Windows.Forms.TextBox txtIntereses;
        private System.Windows.Forms.TextBox txtTasaPorcentual;
        private System.Windows.Forms.CheckBox chkIntereses;
        private System.Windows.Forms.CheckBox chkTasa_Porcentual;
        private System.Windows.Forms.CheckBox chkCapital_Final;
        private System.Windows.Forms.TextBox txtCapitalInicial;
        private System.Windows.Forms.CheckBox chkCapita_Inicial;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private System.Windows.Forms.Button btncalcular;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox txtPeriodo;
        private System.Windows.Forms.ComboBox cmbPeriodo;
    }
}

