﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
namespace Economica
{
    public partial class Formulario : Form
    {
        private static formulas.InteresSimpleFPTI formula;
        public Formulario()
        {
            InitializeComponent();

        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwmd, int wmsg, int wparam, int lparam);


        private void BarraSuperior_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle,0x112,0xf012,0);
        }

        private void BarraSuperior_MouseDown_1(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Object[] periodo = { "Dias", "Meses", "Años" };
            cmbPeriodo.ValueMember = periodo.ToString();
        }

        private void TxtCapitalInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (Char.IsDigit(e.KeyChar))
            //{
            //    e.Handled = false;
            //}else if (Char.IsControl(e.KeyChar))
            //{
            //    e.Handled = false;
            //}
            //else
            //{
            //    e.Handled = true;
            //}

            if ((e.KeyChar == (char)Keys.Enter) || (e.KeyChar == (char)Keys.Return))
            {
                SendKeys.Send("{TAB}");
            }
            else if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && !(e.KeyChar == ('.')))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }else if (e.KeyChar == (char)Keys.Enter)
            {
                SendKeys.Send("{TAB}");//
            }
        }

        private void TxtcapitalFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (Char.IsDigit(e.KeyChar))
            //{
            //    e.Handled = false;
            //}
            //else if (Char.IsControl(e.KeyChar))
            //{
            //    e.Handled = false;
            //}
            //else
            //{
            //    e.Handled = true;
            //}

            if ((e.KeyChar == (char)Keys.Enter) || (e.KeyChar == (char)Keys.Return))
            {
                SendKeys.Send("{TAB}");
            }
            else if(!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && !(e.KeyChar == ('.')))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            else if (e.KeyChar == (char)Keys.Enter)
            {
                SendKeys.Send("{TAB}");//
            }
        }

        private void TxtTasaPorcentual_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (Char.IsDigit(e.KeyChar))
            //{
            //    e.Handled = false;
            //}
            //else if (Char.IsControl(e.KeyChar))
            //{
            //    e.Handled = false;
            //}
            //else
            //{
            //    e.Handled = true;
            //}
            if ((e.KeyChar == (char)Keys.Enter) ||(e.KeyChar == (char)Keys.Return) ) {
                SendKeys.Send("{TAB}");
            } else if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && !(e.KeyChar==('.')))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            //else if (e.KeyChar == (char)Keys.Enter)
            //{
            //    SendKeys.Send("{TAB}");//
            //}
        }

        private void TxtIntereses_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (chkIntereses.Checked)
            {
                
            }
            else
            {
                if ((e.KeyChar == (char)Keys.Enter) || (e.KeyChar == (char)Keys.Return))
                {
                    SendKeys.Send("{TAB}");
                }
                else if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && !(e.KeyChar == ('.')))
                {
                    MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    e.Handled = true;
                    return;
                }
            }
            
        }

        private void ChkCapita_Inicial_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCapita_Inicial.Checked)
            {
                txtCapitalInicial.Enabled = false;
                txtCapitalInicial.BackColor =  Color.FromArgb(170, 175, 179);//= true;
               // txtCapitalInicial.Text = "123456";
            }
            else
            {
                txtCapitalInicial.Enabled = false;
                txtCapitalInicial.BackColor = Color.FromArgb(215, 219, 221);//= true;

            }
        }

        private void ChkCapital_Final_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCapital_Final.Checked)
            {
                txtcapitalFinal.Enabled = false;
                txtcapitalFinal.BackColor = Color.FromArgb(170, 175, 179);//= true;
            }
            else
            {
                txtcapitalFinal.Enabled = false;
                txtcapitalFinal.BackColor = Color.FromArgb(215, 219, 221);//= true;

            }
        }

        private void ChkTasa_Porcentual_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTasa_Porcentual.Checked)
            {
                txtTasaPorcentual.Enabled = false;
                txtTasaPorcentual.BackColor = Color.FromArgb(170, 175, 179);//= true;
            }
            else
            {
                txtTasaPorcentual.Enabled = false;
                txtTasaPorcentual.BackColor = Color.FromArgb(215, 219, 221);//= true;

            }
        }

        private void ChkIntereses_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIntereses.Checked)
            {
                txtIntereses.Enabled = false;
                txtIntereses.BackColor = Color.FromArgb(170, 175, 179);//= true;
            }
            else
            {
                txtIntereses.Enabled = false;
                txtIntereses.BackColor = Color.FromArgb(215, 219, 221);//= true;

            }
        }

        //private CheckBox ListCheckBox()
        //{
        //    //CheckBox all[] = new CheckBox[5];// { chkCapita_Inicial,};


        //}

        private void BtnCalcular_Click(object sender, EventArgs e)
        {
            if (chkCapital_Final.Checked && (txtCapitalInicial.Text !=null) )
            {
                formula = new formulas.InteresSimpleFPTI();
                double vf = formula.ValorFuturo(Double.Parse(txtCapitalInicial.Text),Double.Parse(txtIntereses.Text));
                txtcapitalFinal.Text = vf.ToString();
                //,Double.Parse(txtPeriodo
            }//else if(chek)

        }

        

        private void TxtCapitalInicial_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == (char)Keys.Enter) || (e.KeyChar == (char)Keys.Return))
            {
                SendKeys.Send("{TAB}");
            }
            else if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && !(e.KeyChar == ('.')))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            else if (e.KeyChar == (char)Keys.Enter)
            {
                SendKeys.Send("{TAB}");//
            }
        }

        private void CmbPeriodo_SelectedIndexChanged(object sender, EventArgs e)
        {
          // = false;
        }
    }
}
