﻿namespace Economica
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnFlujo_de_Efectivos = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnFNE = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnTabla_de_Pagos = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnDepreciacioes = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnTMAR_VPN_TIR_IR = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnApplicationExit = new System.Windows.Forms.PictureBox();
            this.btnMinimizedWindows = new System.Windows.Forms.PictureBox();
            this.btnMazimezedWindows = new System.Windows.Forms.PictureBox();
            this.btnApplicationRestoreWindows = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnApplicationExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizedWindows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMazimezedWindows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnApplicationRestoreWindows)).BeginInit();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.btnFlujo_de_Efectivos);
            this.panel1.Location = new System.Drawing.Point(87, 153);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 100);
            this.panel1.TabIndex = 0;
            // 
            // btnFlujo_de_Efectivos
            // 
            this.btnFlujo_de_Efectivos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFlujo_de_Efectivos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(175)))), ((int)(((byte)(179)))));
            this.btnFlujo_de_Efectivos.FlatAppearance.BorderSize = 0;
            this.btnFlujo_de_Efectivos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(219)))), ((int)(((byte)(221)))));
            this.btnFlujo_de_Efectivos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(175)))), ((int)(((byte)(179)))));
            this.btnFlujo_de_Efectivos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFlujo_de_Efectivos.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(0)));
            this.btnFlujo_de_Efectivos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.btnFlujo_de_Efectivos.Image = ((System.Drawing.Image)(resources.GetObject("btnFlujo_de_Efectivos.Image")));
            this.btnFlujo_de_Efectivos.Location = new System.Drawing.Point(0, 0);
            this.btnFlujo_de_Efectivos.Name = "btnFlujo_de_Efectivos";
            this.btnFlujo_de_Efectivos.Size = new System.Drawing.Size(200, 100);
            this.btnFlujo_de_Efectivos.TabIndex = 1;
            this.btnFlujo_de_Efectivos.Text = "Flujo de Efectivos";
            this.btnFlujo_de_Efectivos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFlujo_de_Efectivos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFlujo_de_Efectivos.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.btnFNE);
            this.panel2.Location = new System.Drawing.Point(195, 316);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 100);
            this.panel2.TabIndex = 1;
            // 
            // btnFNE
            // 
            this.btnFNE.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFNE.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(175)))), ((int)(((byte)(179)))));
            this.btnFNE.FlatAppearance.BorderSize = 0;
            this.btnFNE.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(219)))), ((int)(((byte)(221)))));
            this.btnFNE.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(175)))), ((int)(((byte)(179)))));
            this.btnFNE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFNE.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(0)));
            this.btnFNE.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.btnFNE.Image = ((System.Drawing.Image)(resources.GetObject("btnFNE.Image")));
            this.btnFNE.Location = new System.Drawing.Point(0, 0);
            this.btnFNE.Name = "btnFNE";
            this.btnFNE.Size = new System.Drawing.Size(200, 100);
            this.btnFNE.TabIndex = 2;
            this.btnFNE.Text = "Flujo Neto de Efectivos";
            this.btnFNE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFNE.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFNE.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.btnTabla_de_Pagos);
            this.panel3.Location = new System.Drawing.Point(659, 153);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 100);
            this.panel3.TabIndex = 1;
            // 
            // btnTabla_de_Pagos
            // 
            this.btnTabla_de_Pagos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTabla_de_Pagos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(175)))), ((int)(((byte)(179)))));
            this.btnTabla_de_Pagos.FlatAppearance.BorderSize = 0;
            this.btnTabla_de_Pagos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(219)))), ((int)(((byte)(221)))));
            this.btnTabla_de_Pagos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(175)))), ((int)(((byte)(179)))));
            this.btnTabla_de_Pagos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTabla_de_Pagos.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTabla_de_Pagos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.btnTabla_de_Pagos.Image = ((System.Drawing.Image)(resources.GetObject("btnTabla_de_Pagos.Image")));
            this.btnTabla_de_Pagos.Location = new System.Drawing.Point(0, 0);
            this.btnTabla_de_Pagos.Name = "btnTabla_de_Pagos";
            this.btnTabla_de_Pagos.Size = new System.Drawing.Size(200, 100);
            this.btnTabla_de_Pagos.TabIndex = 3;
            this.btnTabla_de_Pagos.Text = "Tabla de Pagos";
            this.btnTabla_de_Pagos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTabla_de_Pagos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTabla_de_Pagos.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(this.btnDepreciacioes);
            this.panel4.Location = new System.Drawing.Point(345, 153);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 100);
            this.panel4.TabIndex = 1;
            // 
            // btnDepreciacioes
            // 
            this.btnDepreciacioes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDepreciacioes.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnDepreciacioes.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(175)))), ((int)(((byte)(179)))));
            this.btnDepreciacioes.FlatAppearance.BorderSize = 0;
            this.btnDepreciacioes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(219)))), ((int)(((byte)(221)))));
            this.btnDepreciacioes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(175)))), ((int)(((byte)(179)))));
            this.btnDepreciacioes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDepreciacioes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDepreciacioes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.btnDepreciacioes.Image = ((System.Drawing.Image)(resources.GetObject("btnDepreciacioes.Image")));
            this.btnDepreciacioes.Location = new System.Drawing.Point(0, 0);
            this.btnDepreciacioes.Name = "btnDepreciacioes";
            this.btnDepreciacioes.Size = new System.Drawing.Size(200, 100);
            this.btnDepreciacioes.TabIndex = 2;
            this.btnDepreciacioes.Text = "Depreciaciones";
            this.btnDepreciacioes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDepreciacioes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDepreciacioes.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.btnTMAR_VPN_TIR_IR);
            this.panel5.Location = new System.Drawing.Point(495, 316);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(200, 100);
            this.panel5.TabIndex = 1;
            // 
            // btnTMAR_VPN_TIR_IR
            // 
            this.btnTMAR_VPN_TIR_IR.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTMAR_VPN_TIR_IR.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(175)))), ((int)(((byte)(179)))));
            this.btnTMAR_VPN_TIR_IR.FlatAppearance.BorderSize = 0;
            this.btnTMAR_VPN_TIR_IR.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(219)))), ((int)(((byte)(221)))));
            this.btnTMAR_VPN_TIR_IR.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(175)))), ((int)(((byte)(179)))));
            this.btnTMAR_VPN_TIR_IR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTMAR_VPN_TIR_IR.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, ((byte)(0)));
            this.btnTMAR_VPN_TIR_IR.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(38)))), ((int)(((byte)(76)))));
            this.btnTMAR_VPN_TIR_IR.Image = ((System.Drawing.Image)(resources.GetObject("btnTMAR_VPN_TIR_IR.Image")));
            this.btnTMAR_VPN_TIR_IR.Location = new System.Drawing.Point(0, 0);
            this.btnTMAR_VPN_TIR_IR.Name = "btnTMAR_VPN_TIR_IR";
            this.btnTMAR_VPN_TIR_IR.Size = new System.Drawing.Size(200, 100);
            this.btnTMAR_VPN_TIR_IR.TabIndex = 2;
            this.btnTMAR_VPN_TIR_IR.Text = "     TMAR, VPN, TIR, IR";
            this.btnTMAR_VPN_TIR_IR.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTMAR_VPN_TIR_IR.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTMAR_VPN_TIR_IR.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(this.btnApplicationRestoreWindows);
            this.panel6.Controls.Add(this.btnMazimezedWindows);
            this.panel6.Controls.Add(this.btnMinimizedWindows);
            this.panel6.Controls.Add(this.btnApplicationExit);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(924, 35);
            this.panel6.TabIndex = 2;
            // 
            // btnApplicationExit
            // 
            this.btnApplicationExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplicationExit.Image = ((System.Drawing.Image)(resources.GetObject("btnApplicationExit.Image")));
            this.btnApplicationExit.Location = new System.Drawing.Point(894, 3);
            this.btnApplicationExit.Name = "btnApplicationExit";
            this.btnApplicationExit.Size = new System.Drawing.Size(25, 25);
            this.btnApplicationExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnApplicationExit.TabIndex = 0;
            this.btnApplicationExit.TabStop = false;
            this.btnApplicationExit.Click += new System.EventHandler(this.BtnApplicationExit_Click);
            // 
            // btnMinimizedWindows
            // 
            this.btnMinimizedWindows.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinimizedWindows.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimizedWindows.Image")));
            this.btnMinimizedWindows.Location = new System.Drawing.Point(832, 3);
            this.btnMinimizedWindows.Name = "btnMinimizedWindows";
            this.btnMinimizedWindows.Size = new System.Drawing.Size(25, 25);
            this.btnMinimizedWindows.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMinimizedWindows.TabIndex = 1;
            this.btnMinimizedWindows.TabStop = false;
            this.btnMinimizedWindows.Click += new System.EventHandler(this.BtnMinimizedWindows_Click);
            // 
            // btnMazimezedWindows
            // 
            this.btnMazimezedWindows.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMazimezedWindows.Image = ((System.Drawing.Image)(resources.GetObject("btnMazimezedWindows.Image")));
            this.btnMazimezedWindows.Location = new System.Drawing.Point(863, 3);
            this.btnMazimezedWindows.Name = "btnMazimezedWindows";
            this.btnMazimezedWindows.Size = new System.Drawing.Size(25, 25);
            this.btnMazimezedWindows.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMazimezedWindows.TabIndex = 2;
            this.btnMazimezedWindows.TabStop = false;
            this.btnMazimezedWindows.Click += new System.EventHandler(this.BtnMazimezedWindows_Click);
            // 
            // btnApplicationRestoreWindows
            // 
            this.btnApplicationRestoreWindows.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplicationRestoreWindows.Image = ((System.Drawing.Image)(resources.GetObject("btnApplicationRestoreWindows.Image")));
            this.btnApplicationRestoreWindows.Location = new System.Drawing.Point(863, 3);
            this.btnApplicationRestoreWindows.Name = "btnApplicationRestoreWindows";
            this.btnApplicationRestoreWindows.Size = new System.Drawing.Size(25, 25);
            this.btnApplicationRestoreWindows.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnApplicationRestoreWindows.TabIndex = 3;
            this.btnApplicationRestoreWindows.TabStop = false;
            this.btnApplicationRestoreWindows.Visible = false;
            this.btnApplicationRestoreWindows.Click += new System.EventHandler(this.BtnApplicationRestoreWindows_Click);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Transparent;
            this.panel7.Controls.Add(this.panel1);
            this.panel7.Controls.Add(this.panel4);
            this.panel7.Controls.Add(this.panel5);
            this.panel7.Controls.Add(this.panel3);
            this.panel7.Controls.Add(this.panel2);
            this.panel7.Location = new System.Drawing.Point(0, 35);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(924, 477);
            this.panel7.TabIndex = 3;
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(924, 512);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Home";
            this.ShowInTaskbar = false;
            this.Text = "Home";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnApplicationExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimizedWindows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMazimezedWindows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnApplicationRestoreWindows)).EndInit();
            this.panel7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnFlujo_de_Efectivos;
        private System.Windows.Forms.Button btnTabla_de_Pagos;
        private System.Windows.Forms.Button btnDepreciacioes;
        private System.Windows.Forms.Button btnFNE;
        private System.Windows.Forms.Button btnTMAR_VPN_TIR_IR;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox btnApplicationRestoreWindows;
        private System.Windows.Forms.PictureBox btnMazimezedWindows;
        private System.Windows.Forms.PictureBox btnMinimizedWindows;
        private System.Windows.Forms.PictureBox btnApplicationExit;
        private System.Windows.Forms.Panel panel7;
    }
}